# sub-1000-lpc-10
ECE 6255 Speech processing project - low rate encoder

# Installation

Download the complete repository and set the src/ folder as the working directory in your MATLAB environment.

The Vector Quantization implementation uses the [VOICEBOX](http://www.ee.ic.ac.uk/hp/staff/dmb/voicebox/voicebox.html#recog)
libarary for MATLAB.

The 'voicebox' directory should be unzipped into the src/ folder.

# Execution

Select a candidate speech file (.wav format) that should be coded, and change the file path in 'main.m'.<br>
Run 'main.m' to produce a corresponding .wav file which is the synthesis result.

# Usage

Frame duration, analysis frame size, sampling frequency, and analysis order must be changed in 'main.m', 'encoder.m', and
'decoder.m' in order to propogate changes to model if desired.

# Notes on Codebook

The VQ system is designed as a flexible unit with sizing and training parameters defined in the 'train_codebook.m' script.

The codebook can be trained by specifying a .wav file path that includes speech suited for training the codebook with a
k-means method (VOICEBOX library). Training iterations can be changed in the 'v_kmeans.m' file (variable 'l') in order to limit
or improve the codebook convergence during training.

The resulting codebook can be found as 'codebook.mat' in the same directory.
To save/load a different codebook by name, make path changes in 'train_codebook.m', 'encoder.m', and 'decoder.m'.

The provided codebook was trained using an ~8 minute sequence of speech snippets provided by [CMU](http://festvox.org/cmu_arctic/).
The sequence was a concatenation of snippets from the US Male (bdl) and Female (clb) speakers.

# Implementation References

The following code sources and datasets were used to form the core implementation of our system:

[VOICEBOX : Speech Processing Toolbox for MATLAB](http://www.ee.ic.ac.uk/hp/staff/dmb/voicebox/voicebox.html#recog)
  <br>Licencing information avaiable at link. Code was not redistributed and can be used as is.</br>
  
[ CMU ARCTIC speech synthesis databases ](http://festvox.org/cmu_arctic/)
  <br>Information on speaking sources and acknowledgements at link. Training set was sequenced manually as in the 'Notes' section.</br>

[ Wideband Speech Coding with Linear Predictive Coding (LPC) ](http://web.uvic.ca/~tyoon/resource/auditorytoolbox/auditorytoolbox/index.html)
  <br>Information on original code source and modifications made by secondary author at link. Our modification and redistribution includes a name change and multiple implementation modifications for bitrate reductions and quality trade-offs.</br>
