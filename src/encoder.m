function [LSF_indices, pitch, G, aperiodicflag, sync] = encoder(superframe, fs, p, preemp)

% This function computes the LPC (linear-predictive coding) coefficients that
% describe a speech signal. The LPC coefficients are a short-time measure of
% the speech signal which describe the signal as the output of an all-pole
% filter. This all-pole filter provides a good description of the speech
% articulators; thus LPC analysis is often used in speech recognition and 
% speech coding systems. The LPC parameters are recalculated, by default in
% this implementation, every 20ms.
%
% The results of LPC analysis are a new representation of the signal
% s(n) = G e(n) - sum from 1 to L a(i)s(n-i)
% where s(n) is the original data. a(i) and e(n) are the outputs of the LPC 
% analysis with a(i) representing the LPC model. The e(n) term represents 
% either the speech source's excitation, or the residual: the details of the 
% signal that are not captured by the LPC coefficients. The G factor is a
% gain term.
%
% LPC analysis is performed on a monaural sound vector (data) which has been
% sampled at a sampling rate of "sr". The following optional parameters modify
% the behaviour of this algorithm.
% L - The order of the analysis. There are L+1 LPC coefficients in the output
% array aCoeff for each frame of data. L defaults to 13.
% fr - Frame time increment, in ms. The LPC analysis is done starting every
% fr ms in time. Defaults to 20ms (50 LPC vectors a second)
% frame - Frame size in ms. The LPC analysis is done by windowing the speech
% data with a rectangular window that is fs ms long. Defaults to 30ms
% preemp - This variable is the epsilon in a digital one-zero filter which 
% serves to preemphasize the speech signal and compensate for the 6dB
% per octave rolloff in the radiation function. Defaults to .9378.
%
% The output variables from this function are
% aCoeff - The LPC analysis results, a(i). One column of L numbers for each
% frame of data
% resid - The LPC residual, e(n). One column of sr*fs samples representing
% the excitation or residual of the LPC filter.
% pitch - A frame-by-frame estimate of the pitch of the signal, calculated
% by finding the peak in the residual's autocorrelation for each frame.
% G - The LPC gain for each frame.
% parcor - The parcor coefficients. The parcor coefficients give the ratio
% between adjacent sections in a tubular model of the speech 
% articulators. There are L parcor coefficients for each frame of 
% speech.
% stream - The LPC analysis' residual or excitation signal as one long vector.
% Overlapping frames of the resid output combined into a new one-
% dimensional signal and post-filtered.
%
% The synlpc routine inverts this transform and returns the original speech
% signal.
%
% This code was graciously provided by:
% Delores Etter (University of Colorado, Boulder) and 
% Professor Geoffrey Orsak (Southern Methodist University) 
% It was first published in
% Orsak, G.C. et al. "Collaborative SP education using the Internet and
% MATLAB" IEEE SIGNAL PROCESSING MAGAZINE Nov. 1995. vol.12, no.6, pp.
% 23-32.
% Modified and debugging plots added by Kate Nguyen and Malcolm Slaney

% A more complete set of routines for LPC analysis can be found at
% http://www.ee.ic.ac.uk/hp/staff/dmb/voicebox/voicebox.html

% (c) 1998 Interval Research Corporation

    fr = 22.5e-3; %frame size in seconds
    LPC_fr = 16.5e-3; %LPC analysis frame size in seconds

    ms_fr = round(fs*fr); % Convert ms to samples
    ms_LPC = round(fs*LPC_fr);

    duration = length(superframe);
    %msoverlap = msfs - msfr;
    %ramp = [0:1/(msoverlap-1):1]'; % Compute part of window

    [aperiodicflag, pitch_est] = voiceNpitch(superframe,fs,ms_fr,ms_LPC,duration,preemp);

    lsfCoeff = []; % location for placing LSF sets for each superframe

    nframe = 0; % used for frame indexing

    for frameIndex=1:ms_fr:duration-ms_fr+1
        nframe = nframe+1;

        frameData = superframe(frameIndex:(frameIndex+ms_LPC-1)); % extract analysis frame

        % Autocorrelation of sequence
        r = xcorr(frameData);
        r = r(ms_LPC:ms_LPC+p);
        % Levinson's method for A(z)
        [A, ~, ~] = levinson(r, p);
        % Creation of LSFs for the set of 4 frames (superframe)
        % aCoeff(:,nframe) = A;
        lsfCoeff(:,nframe) = poly2lsf(A);

        % Gain computation, using model error
        extframeData = superframe(frameIndex:(frameIndex+ms_fr-1)); % exclude analysis frame
        err = filter(A,1,extframeData); % determine model error
        e = sum(err.^2);
        G(nframe) = sqrt(e); % gain using square root error

        % Calculate pitch & voicing information
        err = filter(A, 1, frameData); % model error compared to original frame
        autoCorErr = xcorr(err); 
        [B,I] = sort(autoCorErr);
        num = length(I);

        if B(num-1) > .01*B(num)
            pitch(nframe) = abs(I(num) - I(num-1));
        else
            pitch(nframe) = 0;
        end
    % 
    %     % calculate additional info to improve the compressed sound quality
    %     resid(:,nframe) = errSig/G(nframe);
    %     if(frameIndex==1) % add residual frames using a trapezoidal window
    %     stream = resid(1:msfr,nframe);
    %     else
    %     stream = [stream; 
    %     overlap+resid(1:msoverlap,nframe).*ramp; 
    %     resid(msoverlap+1:msfr,nframe)];
    %     end
    %     if(frameIndex+msfr+msfs-1 > duration)
    %     stream = [stream; resid(msfr+1:msfs,nframe)];
    %     else
    %     overlap = resid(msfr+1:msfs,nframe).*flipud(ramp); 
    %     end 
    end

    % Parameter quantization over four frames
    %
    % Gain
    %
    % Weight vector for averaging gains, sums to 1
    weights = .4:-.1:.1;
    % apply averaging method
    G = dot(weights, sort(G));
    % uniform quantization, 0 : 2^8 - 1
    G = floor( G * ( 2^8 - 1 ) );

    % Pitch
    %
    % averaging method
    pitch = sum( pitch ) / 4;
    % uniform quantization, 0 : 2^9 - 1
    pitch_range = linspace(50, 500, 2^9);
    [~, pitch] = min( abs( pitch_range - pitch ) );

    % aperiodic flag decision
    aperiodicflag = (sum(aperiodicflag)>2);

    %stream = filter(1, [1 -preemp], stream)';

    lsf = (lsfCoeff');

    LSF_indices = quantize_LSFs(lsf);
    
end
