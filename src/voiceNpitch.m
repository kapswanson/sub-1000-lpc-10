%function_main of voiced/unvoiced detection

function [voiced, pitch_est] = voiceNpitch(speech, fs, msfr,msLPC,duration,preemp)

nframe = 0;
magSum = [];  zc = [];  pitch_est = [];
for frameIndex=1:msfr:duration-msfr+1 % frame rate=20ms
    frameData = speech(frameIndex:(frameIndex+msLPC-1)); % frame size=30ms
    
    y = filter([1 -preemp], 1, frameData);  %pre-emphasis filter
    
    nframe = nframe+1;
    [msf,zerocrossing,pitch_period] = pitch_zerocros_magsum(y,fs);
    
    magSum(nframe) = msf;
    zc(nframe) = zerocrossing;
    pitch_est(nframe) = pitch_period;
end
                       
%% Thresholds and Assignments
thresh_msf = (( (sum(magSum)./length(magSum)) - min(magSum)) .* (0.67) ) + min(magSum);
voiced_msf =  magSum > thresh_msf;

thresh_zc = (( ( sum(zc)./length(zc) ) - min(zc) ) .*  (1.5) ) + min(zc);
voiced_zc = zc < thresh_zc;

thresh_pitch = (( (sum(pitch_est)./length(pitch_est)) - min(pitch_est)) .* (0.5) ) + min(pitch_est);
voiced_pitch =  pitch_est > thresh_pitch;



%% Voice or Unvoiced
for b=1:nframe
    if voiced_msf(b) .* voiced_pitch(b) .* voiced_zc(b) == 1
        voiced(b) = 1;
    else
        voiced(b) = 0;
    end
end