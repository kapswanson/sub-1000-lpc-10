function [ LSF_indices ] = quantize_LSFs( LSF_sets )
%QUANTIZE_LSFS Summary of this function goes here
%   Detailed explanation goes here

    FMCQ_CODEBOOK = load('codebook.mat'); % load the codebook into memory
    FMCQ_CODEBOOK = FMCQ_CODEBOOK.codebook;
    
    sz = size(LSF_sets);
    nframe = sz(1);
    
    for i = 1:nframe
        q = sum((LSF_sets(i,:) - FMCQ_CODEBOOK).^2,2);
        [~, index] = sort(q);
        indices = index(1:4);
        frameIndex = indices;
        for k = 1:nframe
            q = sum((FMCQ_CODEBOOK(indices(k),:) - FMCQ_CODEBOOK).^2, 2);
            [~, index] = sort(q);
            frameIndex = [frameIndex,index(2:5)];
        end
        INDEX(:,:,i) = frameIndex;
    end

    indexList = [];
    for i = 1:nframe
        M = INDEX(:,:,i);
        indexList = [];
        for l = 1:nframe
            for j = 1:nframe
                for k = 2:nframe+1
                indexList = [indexList; M(l,1) M(j,k)];
                end
            end
        end
        INDEXLIST(:,:,i) = indexList;
    end

    output_index = [];
    for i = 1:nframe
        error = [];
        currIndexList = INDEXLIST(:,:,i);
        for j =1:length(indexList)
            error = [error; 0.5*(FMCQ_CODEBOOK(currIndexList(j,1),:)+FMCQ_CODEBOOK(currIndexList(j,2),:))];
        end
        q = sum((LSF_sets(i,:) - error).^2,2);
        [~, index] = min(q);
        output_index = [output_index; currIndexList(index,:)];
    end
    
    LSF_indices = output_index;

end

