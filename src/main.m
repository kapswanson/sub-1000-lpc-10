clc;
clear all;
inpfilenm = 'threesentences.wav';
[x,fs] = audioread('threesentences.wav');

% system constants
% ----------------
Order = 10; % order of the model used by LPC
preemp = .9378; %cutoffs 

%
% main
% ----

fr = 22.5e-3; %frame size 
LPCfr = 16.5e-3; %LPC analysis Size;

nframe = 0; 
msfr = round(fs*fr); % Convert ms to samples
msLPC = round(fs*LPCfr); % Convert ms to samples

x = x + .00001;
x = filter([1 -preemp], 1, x)'; % Preemphasize speech

lim = length(x)/msfr;
index = 1; step = 1;
lpcspeech = [];
pitches = [];

for step = 1:lim/4
    superFrame = x(index:index+4*msfr-1);
    [outputind, pitch, G, aperiodicflag] = encoder(superFrame, fs, Order,preemp);
    [outspeech, pitch] = decoder(outputind, pitch, fs, G,aperiodicflag,preemp);
    index = index+4*msfr;
    lpcspeech = [lpcspeech; outspeech];
    pitches = [pitches,pitch];
end

figure
hold on
soundsc(lpcspeech,fs);
hold off

figure;
subplot(2,1,1), plot(x); title(['Original signal = "', inpfilenm, '"']); %title('original signal = "%s"', inpfilenm);
subplot(2,1,2), plot(lpcspeech); title(['synthesized speech of "', inpfilenm, '" using LPC algo']);