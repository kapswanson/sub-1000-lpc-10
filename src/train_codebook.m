% Train an 8-bit vector codebook for quantizing LSFs

% training file
[x,fs] = audioread('audioTrain.wav');

p = 10; % total coefficient order
fr = 22.5e-3; % frame size in seconds
LPC_fr = 16.5e-3; % LPC analysis frame size ( seconds )

ms_fr = round(fs*fr); % Convert ms to samples
ms_LPC = round(fs*LPC_fr);

duration = length(x);

n = 0; % for indexing in output data
for frameIndex=1:ms_fr:duration-ms_fr+1 % frame rate=20ms
    n = n+1;

    frameData = x(frameIndex:(frameIndex+ms_LPC-1)); % extract analysis frame

    % Autocorrelation of sequence
    r = xcorr(frameData);
    r = r(ms_LPC:ms_LPC+p);
    % Levinson's method for obtaining A(z)
    [A, ~, ~] = levinson(r, p);
    % Creation of lsf's
    lsfCoeff = poly2lsf(A);
    lsf(n,:) = lsfCoeff;
end

% generate codebook
[codebook, esq, ~] = kmeanlbg(lsf,2^8);
save codebook.mat codebook
