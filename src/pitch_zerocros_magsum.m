function [magSum,zc,pitch_period] = pitch_zerocros_magsum(y,fs)

magSum = magnitudeSum (y);
zc = zeroCrossings (y);
pitch_period = pitch_est (y,fs);





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This function calculates the zero crossing values
function magSum = magnitudeSum (y)

[B,A] = butter(9,.33,'low');  %.5 or .33?
y1 = filter(B,A,y);

magSum=sum(abs(y1));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This Function calculates the zero crossing
function zc = zeroCrossings (y)
zc = 0;
for n=1:length(y)
    if n+1>length(y)
        break
    end
    zc = zc + 0.5*abs(sign(y(n+1))-sign(y(n)));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function pitch_period = pitch_est (y,fs)

period_min = round (fs*.002); %min period (female voice) = 2ms
period_max = round (fs*.015); %max period (female voice) = 2ms

%BODY OF PROGRAM
R = xcorr(y);

[~, index]=max(R); %R_max = max value of R      %index = index of that value
pitch_per_range = R ( index + period_min : index + period_max );
[~, index] = max(pitch_per_range);
pitch_period = index + period_min;
